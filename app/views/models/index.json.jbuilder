json.array!(@models) do |model|
  json.extract! model, :id, :test_users
  json.url model_url(model, format: :json)
end
